import './style.scss';

let $UlMyList = document.getElementById('myList'),
    $buttomOk = document.getElementById('buttomOk'),
    $newElement = document.getElementById('newElement')
    ;


//Autre façon de déclarer un tableau 
//$UlMyList = new Array();  
let toDoList = [];

// //ft pour ma template qui parcourt le tb
function addElement() {
    $UlMyList.innerHTML = ('');
    toDoList.forEach(element => {

        // Li est un objet lui mettre la className en deuxième étape pour ne pas créer un booléan
        let li = document.createElement('li');
        li.className = "liElement";
        let template = `                    
                        <input type="checkbox">
                        <span class="elementList">${element}</span>
                        <button class="buttonDelete" id="buttomDelete">X</button>
                    `;
        li.innerHTML = template;
        $UlMyList.append(li)

    });
    let $buttonDelete = document.getElementsByClassName('buttonDelete');

    // Supprime l'élément avec la croix
    for (let i = 0; i < $buttonDelete.length; i++) {
        $buttonDelete[i].addEventListener('click', function( event ){
        // Supprime l'élément par rapport à l'index
        toDoList.splice(i, 1);
        // rappeler la ft pour mettre le tb et l'html à jour
        addElement()
        
        
    })}

}

// ajouter les nouveaux éléments dans la tb toDoList
$buttomOk.addEventListener('click', function (event) {
    event.preventDefault();
    let valElement = $newElement.value;
    toDoList.push(valElement);
    addElement()
})

